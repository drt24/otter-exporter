package main

import (
	"bufio"
	"bytes"
	"encoding/csv"
	"flag"
	blackfriday "gopkg.in/russross/blackfriday.v2"
	"io"
	"log"
	"os"
	"strconv"
	"text/template"
)

type Exercise struct {
	Question string
	Answer   string
	Format   string
	Time     int
	Number   int
}

var questionTemplate = template.Must(template.New("question").Parse(
	`## Question {{.Number}}

Estimated completion time: {{.Time}} minutes

{{.Question}}
* * *

`))
var solutionTemplate = template.Must(template.New("solution").Parse(
	`## Question {{.Number}}

Estimated completion time: {{.Time}} minutes

{{.Question}}

### Solution
{{.Answer}}

* * *

`))

func (e Exercise) PrintQuestion(sheet io.Writer) error {
	var buffer bytes.Buffer
	err := questionTemplate.Execute(&buffer, e)
	if err != nil {
		return err
	}
	html := blackfriday.Run(buffer.Bytes())
	html = bytes.Replace(html, []byte("<em>A]</em>"), []byte("_A]_"),-1)
	_, err = sheet.Write(html)
	return err

}
func (e Exercise) PrintSolution(sheet io.Writer) error {
	var buffer bytes.Buffer
	var err error
	if len(e.Answer) > 0 {
		err = solutionTemplate.Execute(&buffer, e)
	} else {
		err = questionTemplate.Execute(&buffer, e)
	}
	if err != nil {
		return err
	}
	html := blackfriday.Run(buffer.Bytes())
	html = bytes.Replace(html, []byte("<em>A]</em>"), []byte("_A]_"),-1)
	_, err = sheet.Write(html)
	return err
}

const opening = `<html>
<head>
<style TYPE="text/css">
code.has-jax {font: inherit; font-size: 100%; background: inherit; border: inherit;}
</style>
<script type="text/x-mathjax-config">
MathJax.Hub.Config({
    tex2jax: {
        inlineMath: [['$','$'], ['\\(','\\)']],
        displayMath: [ ['$$','$$'], ['\[','\]'] ],
        skipTags: ['script', 'noscript', 'style', 'textarea', 'pre'] // removed 'code' entry
    }
});
MathJax.Hub.Queue(function() {
    var all = MathJax.Hub.getAllJax(), i;
    for(i = 0; i < all.length; i += 1) {
        all[i].SourceElement().parentNode.className += ' has-jax';
    }
});
</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.4/MathJax.js?config=TeX-AMS_HTML-full"></script
</head>`
const closeing = "</html>"

func main() {
	flag.Parse()
	args := flag.Args()
	if len(args) != 1 {
		log.Panic("Need 1 argument: csv filename to read ending .csv")
	}
	filename := args[0]
	file, err := os.Open(filename)
	if err != nil {
		log.Panic(err)
	}
	reader := bufio.NewReader(file)
	csvReader := csv.NewReader(reader)
	records, err := csvReader.ReadAll()
	if err != nil {
		log.Panic(err)
	}
	exerciseSheet, err := os.Create(filename[:len(filename)-4] + ".html")
	if err != nil {
		log.Panic(err)
	}
	solutionSheet, err := os.Create(filename[:len(filename)-4] + "_solutions.html")
	if err != nil {
		log.Panic(err)
	}
	_, err = exerciseSheet.WriteString(opening)
	if err != nil {
		log.Panic(err)
	}
	_, err = solutionSheet.WriteString(opening)
	if err != nil {
		log.Panic(err)
	}


	questionNumber := 1
	for _, record := range records {
		question := record[0]
		format := record[1]
		answer := record[2]
		time, err := strconv.Atoi(record[3])
		if err != nil {
			log.Panic(err)
		}
		exercise := Exercise{Question: question, Answer: answer, Format: format, Time: time, Number: questionNumber}
		err = exercise.PrintQuestion(exerciseSheet)
		if err != nil {
			log.Panic(err)
		}
		err = exercise.PrintSolution(solutionSheet)
		if err != nil {
			log.Panic(err)
		}
		questionNumber++
	}
	if err != nil {
		log.Panic(err)
	}
	_, err = exerciseSheet.WriteString(closeing)
	if err != nil {
		log.Panic(err)
	}
	_, err = solutionSheet.WriteString(closeing)
	if err != nil {
		log.Panic(err)
	}
}
